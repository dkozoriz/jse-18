package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.model.Command;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

}