package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.Project;

public interface IProjectRepository extends IAbstractRepository<Project> {


}