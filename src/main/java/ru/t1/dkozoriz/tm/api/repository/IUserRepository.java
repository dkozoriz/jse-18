package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUser(User user);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}