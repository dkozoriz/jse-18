package ru.t1.dkozoriz.tm.api.model;

import ru.t1.dkozoriz.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
