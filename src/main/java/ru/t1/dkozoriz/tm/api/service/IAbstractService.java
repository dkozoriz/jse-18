package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;

import java.util.List;

public interface IAbstractService<T> {

    T create(String name, String description);

    T changeStatusById(String id, Status status);

    T changeStatusByIndex(Integer index, Status status);

    T findOneById(String id);

    T findOneByIndex(Integer index);

    List<T> findAll(Sort sort);

    T updateById(String id, String name, String description);

    T updateByIndex(Integer index, String name, String description);

    T removeById(String id);

    T removeByIndex(Integer index);

    void clear();
}
