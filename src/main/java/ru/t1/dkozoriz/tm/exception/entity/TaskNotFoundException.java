package ru.t1.dkozoriz.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Error! Task not found.");
    }

}