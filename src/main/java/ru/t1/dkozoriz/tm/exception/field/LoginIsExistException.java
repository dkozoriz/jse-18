package ru.t1.dkozoriz.tm.exception.field;

public final class LoginIsExistException extends AbstractFieldException {

    public LoginIsExistException() {
        super("Error! Login is exist.");
    }

}