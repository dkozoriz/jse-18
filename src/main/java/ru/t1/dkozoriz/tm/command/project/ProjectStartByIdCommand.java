package ru.t1.dkozoriz.tm.command.project;

import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "project-start-by-id";

    private static final String DESCRIPTION = "start project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeStatusById(id, Status.IN_PROGRESS);
    }

}
