package ru.t1.dkozoriz.tm.command.project;

import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    private static final String NAME = "project-create";

    private static final String DESCRIPTION = "create new project.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(name, description);
    }

}
