package ru.t1.dkozoriz.tm.command.user;

import ru.t1.dkozoriz.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    private static final String NAME = "view-user-profile";

    private static final String DESCRIPTION = "view profile of current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        final User user = getAuthService().getUser();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("ROLE: " + user.getRole());
    }

}