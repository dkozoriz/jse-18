package ru.t1.dkozoriz.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private static final String NAME = "version";

    private static final String ARGUMENT = "-v";

    private static final String DESCRIPTION = "show version info.";

    public String getName() {
        return NAME;
    }

    public String getArgument() {
        return ARGUMENT;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.18.0");
    }

}
