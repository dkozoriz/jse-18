package ru.t1.dkozoriz.tm.command.user;

import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    private static final String NAME = "change-user-password";

    private static final String DESCRIPTION = "change password of current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        final String userId = getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}