package ru.t1.dkozoriz.tm.command.project;

import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListClearCommand extends AbstractProjectCommand {

    private static final String NAME = "project-clear";

    private static final String DESCRIPTION = "delete all projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }

}
