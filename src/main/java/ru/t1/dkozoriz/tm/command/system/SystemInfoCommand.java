package ru.t1.dkozoriz.tm.command.system;

import static ru.t1.dkozoriz.tm.util.FormatUtil.formatBytes;

public final class SystemInfoCommand extends AbstractSystemCommand {

    private static final String NAME = "info";

    private static final String ARGUMENT = "-i";

    private static final String DESCRIPTION = "show system info.";

    public String getName() {
        return NAME;
    }

    public String getArgument() {
        return ARGUMENT;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorCount);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

}
