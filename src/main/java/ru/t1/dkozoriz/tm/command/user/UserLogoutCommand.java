package ru.t1.dkozoriz.tm.command.user;

import ru.t1.dkozoriz.tm.api.service.IAuthService;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class UserLogoutCommand extends AbstractUserCommand {

    private static final String NAME = "logout";

    private static final String DESCRIPTION = "logout current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

}