package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.service.IUserService;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.entity.UserNotFoundException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return userRepository.add(user);
    }

    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(login)) throw new EmailIsExistException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (role != null ) user.setRole(role);
        return user;
    }

    public User add(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findById(id);
        if (user == null) return null;
        return user;
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        return user;
    }

    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = userRepository.findByEmail(email);
        if (user == null) return null;
        return user;
    }

    public User remove(User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.removeUser(user);
    }

    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        return remove(user);
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        return remove(user);
    }

    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        return remove(user);
    }

    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    public User updateUser(final String id,
                           final String firstName,
                           final String lastName,
                           final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExist(login);
    }

    public Boolean isEmailExist(String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExist(email);
    }

}