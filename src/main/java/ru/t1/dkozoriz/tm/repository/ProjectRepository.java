package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.repository.IProjectRepository;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>(
            Arrays.asList(
                    new Project("Project 1", Status.COMPLETED),
                    new Project("Project 2")
            )
    );

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAll(final Comparator<? super IWBS> comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    public int getSize() {
        return projects.size();
    }

    public void remove(final Project project) {
        if (project == null) return;
        projects.remove(project);
    }

    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

}