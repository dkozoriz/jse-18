package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    public User add(final User user) {
        users.add(user);
        return user;
    }

    public List<User> findAll() {
        return users;
    }

    public User findById(final String id) {
        for (User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    public User findByLogin(final String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    public User findByEmail(final String email) {
        for (User user : users) {
            if (email.equals(user.getLogin())) return user;
        }
        return null;
    }

    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    public boolean isLoginExist(final String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    public boolean isEmailExist(final String email) {
        for (final User user: users) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

}